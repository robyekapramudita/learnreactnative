import React, { useReducer } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const reducer = (state, action) => {
  // state === { count: number }
  // action === { type: 'increment' || 'decrement', payload: }
  
  switch(action.type){
    case 'increment' : 
      return { counter: state.counter + action.payload };
    case 'decrement' :
      return { counter: state.counter - action.payload };
    default : 
      return state;
  }
}

const CounterScreen = () => {
  const [state, runReducer] = useReducer(reducer, { counter: 0 });
  const { counter } = state;

  return (
    <View>
      <Button title="Increase" onPress={() => {
        runReducer({ type: 'increment', payload: 1 });
      }} />
      <Button title="Decrease" onPress={() => {
        runReducer({ type: 'decrement', payload: 1 });
      }} />
      <Text>Current Count : {state.counter}</Text>
    </View>
  )
}

const styles = StyleSheet.create({});

export default CounterScreen; 